# 👋 Hello, 

If you're looking for illustrated backgrounds and banners representing GitLab values(CREDIT), then you've come to the right place 🕵️‍
On this README.md, you'll find the completed illustrations as thumbnails. The WIP items would be added to issue.
To make a request for a specific dimension for an illustration, or even for a whole new illustration, please open an issue!

Remember, `Everyone can contribute`.

```
Disclaimer: Images are brighter than they appear on the README. I don't know why 🙄
```

#### [All the illustrations shared in the project are released under CC0](https://creativecommons.org/publicdomain/zero/1.0/legalcode)  

-------------

The six core values[CREDIT](https://about.gitlab.com/handbook/values/) are what makes GitLab a very unique place to work. They humanize the entire process of working on and developing a tech-product with a team and ensures we don't end up with misplaced priorities on the way.

|  ✨Value |  🎨Artwork(thumbnail) |  💾Download | 👤Attribution |
|--------|---------------------|-------|---------|
| Diversity, Inclusion & Belonging |  ![DIB_thumbnail](/uploads/15da894bc4e16d63e0fbc606ae33a5c2/DIB_thumbnail.jpg)  |  [Images](https://gitlab.com/v_mishra/gitlab-values-illustrations/-/tree/master/Images/DIB)  | ``` <a href='https://gitlab.com/v_mishra/gitlab-values-illustrations/'>Illustration created by Veethika Mishra </a> ``` |
| Transparency |  ![transparency_thumbnail_small](/uploads/2061aad7adae58d09b20ab95c7833eff/transparency_thumbnail_small.jpg)  |  [Images](https://gitlab.com/v_mishra/gitlab-values-illustrations/-/tree/master/Images/Transparency)  | ``` <a href='https://gitlab.com/v_mishra/gitlab-values-illustrations/'>Illustration created by Veethika Mishra </a> ``` |
| Efficiency |  ![Efficiency_thumbnail](/uploads/72a1ffe60eee7684091b7e59236b274c/Efficiency_thumbnail.JPG)  |  [Images](https://gitlab.com/v_mishra/gitlab-values-illustrations/-/tree/master/Images/Efficiency)  | ``` <a href='https://gitlab.com/v_mishra/gitlab-values-illustrations/'>Illustration created by Veethika Mishra </a> ``` |
| Collaboration |  ![Collaboration_thumbnail](/uploads/c2b52f9ec9255e394cb8fb31b5c47d20/Collaboration_thumbnail.JPG)  |  [Images](https://gitlab.com/v_mishra/gitlab-values-illustrations/-/tree/master/Images/Collaboration)  | ``` <a href='https://gitlab.com/v_mishra/gitlab-values-illustrations/'>Illustration created by Veethika Mishra </a> ``` |


--------

# Up next

- 📈 Results
- 👣 Iteration


## Contribution and usage guidelines

### To contribute to this project 
- Look for an issue that's already created for a new brief, or create one yourself describing your idea
- Open a merge request to add the artwork to the repository
- Add a thumbnail to the table in the `README.md` file and also add attribution text in the following format:
```
 <a href='https://gitlab.com/v_mishra/gitlab-values-illustrations/'>Illustration created by <your_name> </a>
```

### To use illustrations in this project
All the illustrations in this project are licensed under [CC0](https://creativecommons.org/share-your-work/public-domain/cc0/), which implies that no rights are reserved by the creator and attribution is not mandated by law. Although, as a courtesy and an appreciation to the effort of the illustrators/creators it is a good practice to include attribution wherever possible.  


----------



